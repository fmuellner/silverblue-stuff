#!/bin/sh

# extra runtime deps
dnf install -y --setopt=install_weak_deps=False dbus-daemon mesa-dri-drivers

# jhbuild sysdeps gets those wrong
dnf install -y --setopt=install_weak_deps=False \
   "pkgconfig(sysprof-capture-3)" asciidoc sassc "pkgconfig(libpulse)" \
   NetworkManager-libnm

rm -rf /tmp
ln -s /run/host/tmp /tmp

rm -rf /run/systemd/{seats,sessions,system,users}
ln -s /run/host/run/systemd/{seats,sessions,system,users} /run/systemd

rm -rf /var/lib/systemd/coredump
ln -s /run/host/var/lib/systemd/coredump /var/lib/systemd/coredump

rm -rf /run/udev
ln -s /run/host/run/udev /run/udev
