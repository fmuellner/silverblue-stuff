# Silverblue stuff

A dumping ground for scripts I use for [GNOME][gnome] hacking
on [Fedora Silverblue][silverblue].

- sb-jhbuild-setup.sh

    Check out and install jhbuild, then install system dependencies for
    a  specified module (or the default `meta-gnome-core`).
    
    Example:
    ```
    toolbox$ ./sb-jhbuild-setup.sh gnome-shell
    ```

- sb-gnome-shell-setup.sh

    Set up a newly created toolbox so that it can run gnome-shell
    as wayland display server.
    
    Run as
    ```
    toolbox$ su -c ./sb-gnome-shell-setup.sh
    ```

- sb-session-setup.sh

    Set up a JHBuild session that can be selected at the login screen.

    Run as
    ```
    host$ ./sb-session-setup.sh
    ```

- jhbuild

    A small convenience wrapper for making the `jhbuild` command available
    outside the corresponding toolbox.

- fedpkg

    The same for the `fedpkg` command ...

- kinit

    ... and kinit

[gnome]: https://www.gnome.org
[silverblue]: https://silverblue.fedoraproject.org/
