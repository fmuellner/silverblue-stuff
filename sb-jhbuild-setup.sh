#!/bin/sh

module=$1

checkout=$HOME/src/jhbuild

su -c "dnf groupinstall -y 'C Development Tools and Libraries'"
su -c "dnf install -y gettext-devel yelp-tools meson"

if [ ! -d $checkout ]; then
    checkout_dir=$(dirname $checkout)
    mkdir -p $checkout_dir
    cd $checkout_dir
    git clone https://gitlab.gnome.org/GNOME/jhbuild.git
fi

cd $checkout
git pull -r

./autogen.sh --prefix /usr && make
su -c "umask 002; make install"

# ffmpeg is now a sysdep
su -c "dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm"

jhbuild sysdeps --dump $module |\
  sed -e /^xml:/d \
      -e /llvm-devel/cllvm-devel \
      -e 's|pkgconfig:\(.*\)|"pkgconfig(\1)"|' \
      -e 's|.*:|"*/"|' > deps

su -c 'dnf install -y --setopt=install_weak_deps=False $(xargs -a deps)'
rm deps
