#!/bin/sh

PREFIX=/usr/local
SESSION_WRAPPER=gnome-jhbuild-session

echo -n Finding jhbuild toolbox ...
jhbuild=
for toolbox in $(podman ps --all --format '{{.Names}}')
do
  toolbox run --container $toolbox which jhbuild >/dev/null

  if [ $? -eq 0 ]
  then
    jhbuild=$toolbox
    break
  fi
done

if [ -z "$jhbuild" ]
then
  echo ' 'not found
  exit 1
fi

echo ' 'found $jhbuild

cat >/tmp/$SESSION_WRAPPER <<-EOT
#!/bin/sh

jhbuild() {
  toolbox run --container $jhbuild jhbuild "\$@"
}

export \$(jhbuild run env | grep JHBUILD_PREFIX= | sed 's:\r::')

USER_UNIT_DIR=\$XDG_RUNTIME_DIR/systemd/user.control

if [ ! -e \$USER_UNIT_DIR ]
then
  # Pick up systemd units defined in jhbuild
  ln -s \$JHBUILD_PREFIX/lib/systemd/user \$USER_UNIT_DIR
  systemctl --user daemon-reload
fi

DBUS_SERVICE_DIR=\$XDG_RUNTIME_DIR/dbus-1

if [ ! -e \$DBUS_SERVICE_DIR ]
then
  ln -s \$JHBUILD_PREFIX/share/dbus-1 \$DBUS_SERVICE_DIR
fi

jhbuild run gnome-session

rm \$DBUS_SERVICE_DIR
rm \$USER_UNIT_DIR
systemctl --user daemon-reload
EOT

echo Installing session wrapper to $PREFIX/bin
sudo install /tmp/$SESSION_WRAPPER $PREFIX/bin

sed '/^Exec/s:=.*:=gnome-jhbuild-session:; /^Name/s:GNOME:GNOME JHBuild:' \
  /usr/share/wayland-sessions/gnome.desktop >/tmp/gnome-jhbuild.desktop

echo Installing wayland session
sudo install -D -t $PREFIX/share/wayland-sessions -m 644 /tmp/gnome-jhbuild.desktop
